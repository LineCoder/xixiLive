package qingyu.ai.common;

/**
 * 全局常量
 */
public interface Constants {
    interface settings {
        String IS_SYNTHES_PLAY = "is_synthes_play";//是否播报
        String WAKE_SWITCH = "wake_switch";//唤醒开关
        String CHECKED_WAKEWORD1 = "checked_wakeword1";//唤醒词
        String CHECKED_WAKEWORD2 = "checked_wakeword2";//唤醒词
        String VOICER = "voicer";
    }

    interface guide {
        String IS_FIRST_USE = "is_first_use";//是否首次使用
    }

    interface net {
        String DEFAULT_REFRESH_TOKEN = "OTk1YmM0NmY5ZGFmOWM4ZWQ5NDY3OGQwN2Y5MTI2ZjE=";
        int NONET = 10005;//无网络
        int SUCCESS = 200;//请求成功
        int NETERROR = 10003;//网络异常
        int UNKONWERROR = 2020;//未知错误

        int LOCAL_TOKEN_INVALID = 403;//token失效，需要刷新token
        int ACCOUNT_EXISTED = 10001;//账号已存在
        int ACCOUNT_FORMAT_WRONG = 10002;//手机/邮箱格式不正确
        int ACCOUNT_NOT_EXISTED = 10003;//账号不存在
        int VERIFICATION_CODE_INVALID = 10004;//验证码不存在或已失效
        int VERIFICATION_CODE_WRONG = 10005;//验证码不正确
        int VERIFICATION_CODE_TOO_MORE = 10006;//验证码发送过于频繁，请稍后重试
        int USER_NOT_EXIST = 10007;//验证码发送过于频繁，请稍后重试
        int OLD_PASSWORD_WRONG = 10008;//原密码不正确
        int REFRASH_TOKEN_FAILED = 10009;//刷新票据失败，需要用户重新登录
        int ACCOUNTORPASSWORD_NOTEXIST = 10010;//账号或密码不正确
    }

    interface userinfo {
        String account = "account";
        String iconUrl = "icon_url";//头像Url
        String access_token = "access_token";//访问token
        String refresh_token = "refresh_token";
        String nickname = "nickname";
    }
}
