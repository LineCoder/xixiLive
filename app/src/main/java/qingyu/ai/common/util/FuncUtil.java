package qingyu.ai.common.util;

import android.net.Uri;
import android.text.TextUtils;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

/**
 * 功能拓展工具类
 */
public class FuncUtil {
    /**
     * 替换手机号
     *
     * @param oldPhoneNum
     * @return
     */
    public static String replacePhoneNumber(String oldPhoneNum) {
        StringBuilder sb = new StringBuilder(oldPhoneNum);
        if (RegexUtils.isMobileExact(oldPhoneNum)) {
            sb.replace(3, 7, "****");
        }
        return sb.toString();
    }

    /**
     * 用户头像，如果没有iconUrl则使用默认头像
     *
     * @param user_avatar
     * @param iconUrl
     * @param defResId
     */
    public static void defaultOrUserIcon(SimpleDraweeView user_avatar, String iconUrl, int defResId) {
        if (!TextUtils.isEmpty(iconUrl)) {
            user_avatar.setImageURI(Uri.parse(iconUrl));
        } else {
            user_avatar.setImageResource(defResId);
        }
    }

    /**
     * 用户昵称
     *
     * @param tv_nick_name
     * @param nick_name
     * @param def_nick_name
     */
    public static void defaultOrUserNickName(TextView tv_nick_name, String nick_name, int def_nick_name) {
        if (!TextUtils.isEmpty(nick_name)) {
            tv_nick_name.setText(nick_name);
        } else {
            tv_nick_name.setText(def_nick_name);
        }
    }
}
