package qingyu.ai.common.util;

import android.os.CountDownTimer;
import android.widget.Button;

public class CountDownButtonHolder extends CountDownTimer {
    private Button button;
    private String defaultString;

    public CountDownButtonHolder(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    /**
     * @param button        需要显示倒计时的Button
     * @param defaultString 默认显示的字符串
     * @param max           需要进行倒计时的最大值,单位是秒
     * @param interval      倒计时的间隔，单位是秒
     */
    public CountDownButtonHolder(final Button button, final String defaultString, int max, int interval) {
        this(max * 1000, interval * 1000 - 10);
        this.button = button;
        this.defaultString = defaultString;
    }


    public void startCD() {
        super.start();
        button.setEnabled(false);
    }

    public void cancelCD() {
        super.cancel();
        button.setEnabled(true);
        button.setText(defaultString);
    }

    @Override
    public void onTick(long time) {
        button.setText(defaultString + "(" + ((time + 15) / 1000) + "秒)");
    }

    @Override
    public void onFinish() {
        button.setEnabled(true);
        button.setText(defaultString);
    }
}
