package qingyu.ai.common.util;

import java.io.File;

/**
 * Created by wfy on 2018/3/14.
 */

public interface CliperListener {
    void onResponse(File file);
}
