package qingyu.ai.common.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import qingyu.ai.GlobalApp;

/**
 * sp工具类:存放于app设置相关数据：
 */
public class SPUtil {
    private static final String FILE_NAME = "config";
    private static SharedPreferences mPref = GlobalApp.getAppContext().getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);

    private SPUtil() {
        throw new RuntimeException("SPUtil not create one instance");
    }

    public static void putBoolean(String key, boolean value) {
        mPref.edit().putBoolean(key, value).commit();
    }

    public static boolean getBoolean(String key, boolean defVal) {
        return mPref.getBoolean(key, defVal);
    }

    public static String getString(String key, String defVal) {
        return mPref.getString(key, defVal);
    }

    public static boolean putString(String key, String defVal) {
        return mPref.edit().putString(key, defVal).commit();
    }

    public static boolean remove(String key) {
        return mPref.edit().remove(key).commit();
    }

    public static void clear() {
        mPref.edit().clear().commit();
    }
}
