package qingyu.ai.common.speech.synthesizer;

import android.os.Bundle;

import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SynthesizerListener;

public class SynthesizerAdapter implements SynthesizerListener {
    private SynthesizerCallback callback;

    public SynthesizerAdapter(SynthesizerCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onSpeakBegin() {

    }

    @Override
    public void onBufferProgress(int i, int i1, int i2, String s) {

    }

    @Override
    public void onSpeakPaused() {

    }

    @Override
    public void onSpeakResumed() {

    }

    @Override
    public void onSpeakProgress(int i, int i1, int i2) {

    }

    @Override
    public void onCompleted(SpeechError speechError) {
        if (callback != null)
            callback.onCompleted(speechError);
    }

    @Override
    public void onEvent(int i, int i1, int i2, Bundle bundle) {

    }
}
