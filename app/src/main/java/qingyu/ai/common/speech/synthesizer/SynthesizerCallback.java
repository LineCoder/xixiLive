package qingyu.ai.common.speech.synthesizer;

import com.iflytek.cloud.SpeechError;

/**
 * Created by wfy on 2018/3/20.
 */

public interface SynthesizerCallback {
    void onCompleted(SpeechError speechError);
}
