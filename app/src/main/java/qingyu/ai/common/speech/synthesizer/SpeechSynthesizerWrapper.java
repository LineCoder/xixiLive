package qingyu.ai.common.speech.synthesizer;

import android.content.Context;
import android.media.AudioManager;
import android.text.TextUtils;

import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SynthesizerListener;

import static com.iflytek.cloud.SpeechConstant.TYPE_CLOUD;

/**
 * <p>
 * 语音合成:
 * <p>
 * 设计为单列模式
 */
public class SpeechSynthesizerWrapper {
    public static String mSpeakText;
    private static volatile SpeechSynthesizerWrapper mInstance;
    private SpeechSynthesizer mSpeechSynthesizer;
    private boolean isPause = false;

    public static class SynthesizerConstant {
        public static final String PITCH = "50";/*音调*/
        public static final String VOLUME = "100";//音量
        public static final String STREAM_TYPE = AudioManager.STREAM_MUSIC + "";
        public static final String SAMPLE_8K = "8000";//采样率值
        public static final String SAMPLE_16K = "16000";//采样率值
        public static final String EMOT = "neutral";//值范围：{ neutral, happy, sad, angry }。
        public static final String FADING = "1";
        public static final String BUFFER_TIME = "150";
    }

    private SpeechSynthesizerWrapper(Context context) {
        if (mInstance != null)
            throw new RuntimeException("SpeechSynthesizerWrapper can only b one instance");

        mSpeechSynthesizer = SpeechSynthesizer.createSynthesizer(context, mInitListener);
    }

    public static SpeechSynthesizerWrapper createSynthesizerCommond(Context context) {
        if (null == mInstance) {
            synchronized (SpeechSynthesizerWrapper.class) {
                if (null == mInstance) {
                    mInstance = new SpeechSynthesizerWrapper(context);
                }
            }
        }
        return mInstance;
    }


    public void startSpeaking(String speakingText, SynthesizerListener listener) {
        SpeechSynthesizer
                .getSynthesizer().startSpeaking(speakingText, listener);
    }


    /**
     * 取消合成
     * <p>
     * 停止合成 调用此函数，取消当前合成会话，并停止音频播放。
     */

    public SpeechSynthesizerWrapper stopSpeaking() {
        if (null != mSpeechSynthesizer) {
            if (mSpeechSynthesizer.isSpeaking()) {
                synchronized (SpeechSynthesizerWrapper.class) {
                    if (mSpeechSynthesizer.isSpeaking()) {
                        mSpeechSynthesizer.stopSpeaking();
                    }
                }
            }
        }
        return mInstance;
    }


    /**
     * 语音合成监听
     *
     * @return
     */
    public boolean isSpeaking() {
        return mSpeechSynthesizer != null && mSpeechSynthesizer.isSpeaking();
    }

    /**
     * 销毁单例对象 通过本函数，
     * 销毁由createSynthesizer(Context, com.iflytek.cloud.InitListener)创建的单例对象。
     */
    public void destroy() {
        if (mSpeechSynthesizer != null) {
            if (mSpeechSynthesizer.isSpeaking()) {
                mSpeechSynthesizer.stopSpeaking();
            }
            mSpeechSynthesizer.destroy();
            mInstance = null;
        }
    }

    /**
     * 获取单例对象 通过函数获取已创建的单例对象。
     *
     * @return
     */
    public SpeechSynthesizer getSynthesizer() {
        return SpeechSynthesizer.getSynthesizer();
    }

    /**
     * 使用默认请传""
     *
     * @param voicer 发音人
     * @param volume 音量
     * @param speed  语速
     */
    public SpeechSynthesizerWrapper initReadParams(String voicer, String volume, String speed, String bufferTime) {

        if (mSpeechSynthesizer != null) {
            // 清空参数
            mSpeechSynthesizer.setParameter(SpeechConstant.PARAMS, null);
            //设置使用云端引擎
            mSpeechSynthesizer.setParameter(SpeechConstant.ENGINE_TYPE, TYPE_CLOUD);
            //设置发音人
            if (TextUtils.isEmpty(voicer)) {
                voicer = "vinn";
            }
            mSpeechSynthesizer.setParameter(SpeechConstant.VOICE_NAME, voicer);
            //网络连接超时时间
            mSpeechSynthesizer.setParameter(SpeechConstant.NET_TIMEOUT, "10000");

            //设置合成语速
            if (TextUtils.isEmpty(speed)) {
                speed = "75";
            }
            //通过此参数，在合成中使用不同的情感。
            mSpeechSynthesizer.setParameter(SpeechConstant.EMOT, SynthesizerConstant.EMOT);
            mSpeechSynthesizer.setParameter(SpeechConstant.SPEED, speed);
            //设置合成音调
            mSpeechSynthesizer.setParameter(SpeechConstant.PITCH, SynthesizerConstant.PITCH);
            //设置合成音量
            mSpeechSynthesizer.setParameter(SpeechConstant.VOLUME, SynthesizerConstant.VOLUME);
            //设置播放器音频流类型
            mSpeechSynthesizer.setParameter(SpeechConstant.STREAM_TYPE, SynthesizerConstant.STREAM_TYPE);
            // 设置播放合成音频打断音乐播放，默认为true
            mSpeechSynthesizer.setParameter(SpeechConstant.KEY_REQUEST_FOCUS, "false");

            if (!TextUtils.isEmpty(bufferTime.trim())) {
                mSpeechSynthesizer.setParameter(SpeechConstant.TTS_BUFFER_TIME, bufferTime);
            } else {
                mSpeechSynthesizer.setParameter(SpeechConstant.TTS_BUFFER_TIME, SynthesizerConstant.BUFFER_TIME);
            }

            // 采样率
            mSpeechSynthesizer.setParameter(SpeechConstant.SAMPLE_RATE, SynthesizerConstant.SAMPLE_16K);
            mSpeechSynthesizer.setParameter(SpeechConstant.TTS_FADING, SynthesizerConstant.FADING);

        }
        return mInstance;
    }

    /**
     * 初始化监听接口
     */
    private InitListener mInitListener = new InitListener() {
        @Override
        public void onInit(int code) {
        }
    };
}
