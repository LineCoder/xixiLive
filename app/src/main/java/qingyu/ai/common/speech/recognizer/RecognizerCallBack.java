package qingyu.ai.common.speech.recognizer;

public interface RecognizerCallBack {
    void onRecognizerResult(String result);

    void onError(int errCode, String errMsg);
}
