package qingyu.ai.common.net;

import android.content.Context;

import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;
import qingyu.ai.domain.HttpResult;
import qingyu.ai.domain.PayloadBean;
import qingyu.ai.domain.TuLin;
import qingyu.ai.domain.TuLinResult;
import qingyu.ai.domain.UserInfoData;

public class APIFactory extends RetrofitHttpUtil {
    public APIFactory(Context context, String baseUrl) {
        init(context, baseUrl);
    }

    /**
     * 发送注册验证码
     *
     * @param params
     * @param calback
     */
    public void getRegisterCode(final Map<String, String> params, final NetRequestAdapter<PayloadBean> calback) {
        Flowable<PayloadBean> registerCode = apiService
                .getRegisterCode(params)
                .map(new HttpResultFunc<PayloadBean>());
        toSubscribe(registerCode, calback);
    }

    /**
     * 注册
     *
     * @param map
     * @param calback
     */
    public void register(Map<String, String> map, NetRequestAdapter<UserInfoData> calback) {
        Flowable<UserInfoData> dataFlowable = apiService
                .register(map)
                .map(new HttpResultFunc<UserInfoData>());
        toSubscribe(dataFlowable, calback);
    }

    /**
     * 登陆
     *
     * @param map
     * @param calback
     */
    public void login(Map<String, String> map, NetRequestAdapter<UserInfoData> calback) {
        Flowable<UserInfoData> login = apiService
                .login(map)
                .map(new HttpResultFunc<UserInfoData>());
        toSubscribe(login, calback);
    }

    /**
     * QQ登陆
     *
     * @param map
     * @param calback
     */
    public void qqLogin(Map<String, String> map, NetRequestAdapter<UserInfoData> calback) {
        Flowable<UserInfoData> login = apiService
                .qqLogin(map)
                .map(new HttpResultFunc<UserInfoData>());
        toSubscribe(login, calback);
    }

    /**
     * 退出登陆
     *
     * @param map
     * @param calback
     */
    public void logout(Map<String, String> map, NetRequestAdapter<UserInfoData> calback) {
        Flowable<UserInfoData> login = apiService
                .logout(map)
                .map(new HttpResultFunc<UserInfoData>());
        toSubscribe(login, calback);
    }

    /**
     * 刷新token
     *
     * @param map
     * @return
     */
    public retrofit2.Call<HttpResult<UserInfoData>> refrashToken(Map<String, String> map) {
        return apiService.refreshToken(map);
    }

    /**
     * 发送忘记密码验证码
     *
     * @param params
     * @param calback
     */
    public void getForGetPassCode(Map<String, String> params, final NetRequestAdapter<PayloadBean> calback) {
        Flowable<PayloadBean> flowable = apiService.getForGetPassCode(params)
                .map(new HttpResultFunc<PayloadBean>());
        toSubscribe(flowable, calback);
    }

    /**
     * 找回密码
     *
     * @param map
     * @param calback
     */
    public void forget(Map<String, String> map, NetRequestAdapter<UserInfoData> calback) {
        Flowable<UserInfoData> dataFlowable = apiService
                .forget(map)
                .map(new HttpResultFunc<UserInfoData>());
        toSubscribe(dataFlowable, calback);
    }

    /**
     * 修改用户资料
     *
     * @param map
     * @param calback
     */
    public void updateUserInfo(Map<String, String> map, NetRequestAdapter<UserInfoData> calback) {
        Flowable<UserInfoData> flowable = apiService
                .updateUserInfo(map)
                .map(new HttpResultFunc<UserInfoData>());
        toSubscribe(flowable, calback);
    }

    /**
     * 修改用户密码
     *
     * @param map
     * @param calback
     */
    public void updatePassword(Map<String, String> map, NetRequestAdapter<UserInfoData> calback) {
        Flowable<UserInfoData> flowable = apiService.updatePassword(map)
                .map(new HttpResultFunc<UserInfoData>());
        toSubscribe(flowable, calback);
    }


    /**
     * 绑定账号
     *
     * @param map
     * @param calback
     */
    public void bindAccount(Map<String, String> map, NetRequestAdapter<UserInfoData> calback) {
        Flowable<UserInfoData> dataFlowable = apiService
                .bindAccount(map)
                .map(new HttpResultFunc<UserInfoData>());
        toSubscribe(dataFlowable, calback);
    }


    /**
     * 发送注册验证码
     *
     * @param params
     * @param calback
     */
    public void getBindAccountCode(final Map<String, String> params, final NetRequestAdapter<PayloadBean> calback) {
        Flowable<PayloadBean> registerCode = apiService
                .getBindAccountCode(params)
                .map(new HttpResultFunc<PayloadBean>());
        toSubscribe(registerCode, calback);
    }

    public void tulin(TuLin tuLin, NetRequestAdapter<TuLinResult> callback) {
        Flowable<TuLinResult> map = apiService.tulin(tuLin)
                .map(new Function<TuLinResult, TuLinResult>() {
                    @Override
                    public TuLinResult apply(TuLinResult tuLinResult) throws Exception {
                        return tuLinResult;
                    }
                });
        toSubscribe(map, callback);
    }

}
