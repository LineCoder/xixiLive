package qingyu.ai.common.net;

import java.util.Map;

import io.reactivex.Flowable;
import qingyu.ai.domain.HttpResult;
import qingyu.ai.domain.PayloadBean;
import qingyu.ai.domain.TuLin;
import qingyu.ai.domain.TuLinResult;
import qingyu.ai.domain.UserInfoData;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * 网络请求服务接口
 */
public interface APIService {
    @GET("message/regist")
    Flowable<HttpResult<PayloadBean>> getRegisterCode(@QueryMap Map<String, String> params);//请求注册验证码

    @FormUrlEncoded
    @POST("regist")
    Flowable<HttpResult<UserInfoData>> register(@FieldMap Map<String, String> map);//注册

    @FormUrlEncoded
    @POST("login")
    Flowable<HttpResult<UserInfoData>> login(@FieldMap Map<String, String> map);//登陆

    @FormUrlEncoded
    @POST("login/qq")
    Flowable<HttpResult<UserInfoData>> qqLogin(@FieldMap Map<String, String> map);//QQ登陆

    @FormUrlEncoded
    @POST("exit")
    Flowable<HttpResult<UserInfoData>> logout(@FieldMap Map<String, String> token);//退出登陆

    @FormUrlEncoded
    @POST("refresh_token")
    Call<HttpResult<UserInfoData>> refreshToken(@FieldMap Map<String, String> token);//刷新token

    @GET("message/forget")
    Flowable<HttpResult<PayloadBean>> getForGetPassCode(@QueryMap Map<String, String> params);//请求忘记密码验证码

    @FormUrlEncoded
    @POST("forget")
    Flowable<HttpResult<UserInfoData>> forget(@FieldMap Map<String, String> map);//找回密码

    @FormUrlEncoded
    @POST("profile/update")
    Flowable<HttpResult<UserInfoData>> updateUserInfo(@FieldMap Map<String, String> map);//修改用户资料

    @FormUrlEncoded
    @POST("password/update")
    Flowable<HttpResult<UserInfoData>> updatePassword(@FieldMap Map<String, String> map);//修改用户密码

    @FormUrlEncoded
    @POST("account/bind")
    Flowable<HttpResult<UserInfoData>> bindAccount(@FieldMap Map<String, String> map);//绑定

    @GET("message/bind")
    Flowable<HttpResult<PayloadBean>> getBindAccountCode(@QueryMap Map<String, String> params);//请求绑定验证码

    @POST("http://www.tuling123.com/openapi/api")
    Flowable<TuLinResult> tulin(@Body TuLin tuLin);//图灵
}
