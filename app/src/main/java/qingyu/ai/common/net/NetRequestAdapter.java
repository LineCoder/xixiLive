package qingyu.ai.common.net;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import qingyu.ai.common.Constants;

/**
 * 适配器模式：
 * 将Subscriber适配为NetworkRequestCalback
 *
 * @param <T>
 */
public class NetRequestAdapter<T> implements Subscriber<T> {
    private NetworkRequestCalback calback;

    public NetRequestAdapter(NetworkRequestCalback calback) {
        this.calback = calback;
    }

    /**
     * 主线程执行
     *
     * @param s
     */
    @Override
    public void onSubscribe(Subscription s) {
        s.request(1);
        if (calback != null)
            calback.onNetStart();
    }

    /**
     * 主线程执行
     *
     * @param s
     */
    @Override
    public void onError(Throwable throwable) {
        if (throwable instanceof SocketTimeoutException) {
            if (calback != null) {
                calback.onError(Constants.net.NETERROR, "网络中断，请检查您的网络状态");
            }
        } else if (throwable instanceof ConnectException) {
            if (calback != null) {
                calback.onError(Constants.net.NETERROR, "网络中断，请检查您的网络状态");
            }
        } else if (throwable instanceof UnknownHostException) {
            if (calback != null) {
                calback.onError(Constants.net.NETERROR, "网络中断，请检查您的网络状态");
            }
        } else if (throwable instanceof APIException) {
            if (calback != null) {
                calback.onError(((APIException) throwable).getCode(), ((APIException) throwable).getMessage());
            }
        } else {
            if (calback != null) {
                calback.onError(Constants.net.UNKONWERROR, "操作失败，请重试");
            }
        }
        throwable.printStackTrace();
    }

    /**
     * 主线程执行
     *
     * @param s
     */
    @Override
    public void onNext(T t) {
        if (calback != null)
            calback.onSuccess(t);
    }

    /**
     * 注意：
     * 主线程执行
     *
     * @param <p> 通过观看源码可知出现error不会回调此方法，只有至在onNext方法调用完成才会调用此方法
     */
    @Override
    public void onComplete() {
        if (calback != null)
            calback.onNetFinish();
    }
}
