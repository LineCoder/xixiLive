package qingyu.ai.common.net;

public interface NetworkRequestCalback<T> {
    void onSuccess(T data);

    void onError(int errorCode, String erorMsg);

    void onNetStart();

    void onNetFinish();
}
