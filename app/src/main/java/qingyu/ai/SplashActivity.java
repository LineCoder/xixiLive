package qingyu.ai;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import qingyu.ai.common.Constants;
import qingyu.ai.common.util.SPUtil;
import qingyu.ai.ui.guide.GuideActivity;
import qingyu.ai.ui.main.MainActivity;

public class SplashActivity extends AppCompatActivity implements Animation.AnimationListener {

    @BindView(R.id.iv_splash)
    ImageView iv_splash;
    private Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //将window的背景图设置为空
        getWindow().setBackgroundDrawable(null);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        animation = AnimationUtils.loadAnimation(this, R.anim.anim_root_act);
        animation.setFillAfter(true);
        animation.setAnimationListener(this);
        iv_splash.startAnimation(animation);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        boolean isFirstUse = SPUtil.getBoolean(Constants.guide.IS_FIRST_USE, true);
        Log.e("xixi", isFirstUse + "");
        if (!isFirstUse) {
            startActivity(new Intent(this, MainActivity.class));
        } else {
            startActivity(new Intent(this, GuideActivity.class));
        }
        finish();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onBackPressed() {
    }
}
