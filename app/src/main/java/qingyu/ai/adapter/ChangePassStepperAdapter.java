package qingyu.ai.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import qingyu.ai.ui.safe.password.ChangePassFragmentOne;
import qingyu.ai.ui.safe.password.ChangePassFragmentTwo;

public class ChangePassStepperAdapter extends AbstractFragmentStepAdapter {
    public ChangePassStepperAdapter(@NonNull FragmentManager fm, @NonNull Context context) {
        super(fm, context);
    }


    @NonNull
    @Override
    public StepViewModel getViewModel(int position) {
        StepViewModel.Builder builder = new StepViewModel.Builder(context);
        switch (position) {
            case 0:
                builder.setEndButtonLabel("填写旧密码");
                builder.setTitle("填写旧密码");
                break;
            case 1:
                builder.setEndButtonLabel("完成并登录");
                builder.setBackButtonLabel("上一步");
                builder.setTitle("填写新密码");
                break;
        }
        return builder.create();
    }

    @Override
    public Step createStep(int position) {
        switch (position) {
            case 0:
                return new ChangePassFragmentOne();
            case 1:
                return new ChangePassFragmentTwo();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
