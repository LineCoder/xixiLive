package qingyu.ai.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import qingyu.ai.ui.safe.forget.ForgetFragmentOne;
import qingyu.ai.ui.safe.forget.ForgetFragmentThree;
import qingyu.ai.ui.safe.forget.ForgetFragmentTwo;

public class ForgetStepperAdapter extends AbstractFragmentStepAdapter {

    /**
     *0:register
     *
     */
    protected int type;

    public ForgetStepperAdapter(@NonNull FragmentManager fm, @NonNull Context context) {
        super(fm, context);
    }


    @NonNull
    @Override
    public StepViewModel getViewModel(int position) {
        StepViewModel.Builder builder = new StepViewModel.Builder(context);
        switch (position) {
            case 0:
                builder.setEndButtonLabel("获取验证码");
                builder.setTitle("填写账号");
                break;
            case 1:
                builder.setEndButtonLabel("设置密码");
                builder.setBackButtonLabel("上一步");
                builder.setTitle("填写验证码");
                break;
            case 2:
                builder.setEndButtonLabel("完成并登录");
                builder.setBackButtonLabel("上一步");
                builder.setTitle("填写密码");
                break;
        }
        return builder.create();
    }

    @Override
    public Step createStep(int position) {
        switch (position) {
            case 0:
                return new ForgetFragmentOne();
            case 1:
                return new ForgetFragmentTwo();
            case 2:
                return new ForgetFragmentThree();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
