package qingyu.ai;

import android.app.Application;
import android.content.Context;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;
import com.vondear.rxtools.RxTool;

public class GlobalApp extends Application {
    private static Context mContext;
    public static String account;//手机号
    public static String code;//验证码
    public static String oldpass;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        Fresco.initialize(this);
        RxTool.init(this);
        StringBuffer param = new StringBuffer();
        param.append("appid=").append(getString(R.string.app_id));
        param.append(",");
        // 设置使用v5+
        param.append(SpeechConstant.ENGINE_MODE + "=" + SpeechConstant.MODE_MSC);
        SpeechUtility.createUtility(this.getApplicationContext(), param.toString());
    }

    public static Context getAppContext() {
        return mContext;
    }
}
