package qingyu.ai.domain;

import qingyu.ai.common.Constants;

public class HttpResult<T> {
    public int code;
    public String message;
    public T payload;
    public boolean status;

    public boolean isSuccess() {
        return code == Constants.net.SUCCESS;
    }
}
