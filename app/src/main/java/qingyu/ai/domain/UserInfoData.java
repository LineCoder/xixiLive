package qingyu.ai.domain;

public class UserInfoData {
    private MemberBean member;
    private ResultBean result;

    public MemberBean getMember() {
        return member;
    }

    public void setMember(MemberBean member) {
        this.member = member;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class MemberBean {
        private String account;
        private String avatar;
        private String birthday;
        private int member_id;
        private String nickname;
        private int sex;

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getBirthday() {
            return birthday;
        }

        public void setBirthday(String birthday) {
            this.birthday = birthday;
        }

        public int getMember_id() {
            return member_id;
        }

        public void setMember_id(int member_id) {
            this.member_id = member_id;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public int getSex() {
            return sex;
        }

        public void setSex(int sex) {
            this.sex = sex;
        }

        @Override
        public String toString() {
            return "MemberBean{" +
                    "account=" + account +
                    ", avatar=" + avatar +
                    ", birthday=" + birthday +
                    ", member_id=" + member_id +
                    ", nickname='" + nickname + '\'' +
                    ", sex=" + sex +
                    '}';
        }
    }

    public static class ResultBean {
        private String access_token;
        private int expire;
        private String refresh_token;

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }

        public int getExpire() {
            return expire;
        }

        public void setExpire(int expire) {
            this.expire = expire;
        }

        public String getRefresh_token() {
            return refresh_token;
        }

        public void setRefresh_token(String refresh_token) {
            this.refresh_token = refresh_token;
        }

        @Override
        public String toString() {
            return "ResultBean{" +
                    "access_token='" + access_token + '\'' +
                    ", expire=" + expire +
                    ", refresh_token='" + refresh_token + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "UserInfoData{" +
                "member=" + member +
                ", result=" + result +
                '}';
    }
}
