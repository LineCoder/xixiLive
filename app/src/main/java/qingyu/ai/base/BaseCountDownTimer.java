package qingyu.ai.base;

import android.os.CountDownTimer;

public class BaseCountDownTimer extends CountDownTimer {

    public BaseCountDownTimer(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }


    public BaseCountDownTimer(long millisInFuture) {
        super(millisInFuture, 1000);
    }

    @Override
    public void onTick(long millisUntilFinished) {
    }

    @Override
    public void onFinish() {
    }
}
