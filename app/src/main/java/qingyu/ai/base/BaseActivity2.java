package qingyu.ai.base;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import qingyu.ai.GlobalApp;
import qingyu.ai.R;
import qingyu.ai.common.util.ActManager;

/**
 * 事件和提醒
 */
public class BaseActivity2 extends AppCompatActivity {

    @BindView(R.id.toolbar_text)
    TextView toolbar_text;

    @BindView(R.id.navbar_icon_back)
    ImageView navbar_icon_back;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        ActManager.getAppManager().addActivity(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        ButterKnife.bind(this);
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
        ButterKnife.bind(this);
    }

    public void initToolBar(String title, int iconResId) {
        if (iconResId > 0)
            this.navbar_icon_back.setImageResource(iconResId);
        else
            this.navbar_icon_back.setImageResource(R.mipmap.navbar_icon_back);
        toolbar_text.setText(title);
    }

    @OnClick(R.id.navbar_icon_back)
    public void back() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActManager.getAppManager().finishAct(this);
    }
}
