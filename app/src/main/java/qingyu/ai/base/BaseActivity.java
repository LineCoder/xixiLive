package qingyu.ai.base;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import qingyu.ai.GlobalApp;
import qingyu.ai.R;
import qingyu.ai.common.util.ActManager;

/**
 * 事件和提醒
 */
public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_text)
    TextView toolbar_text;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        ActManager.getAppManager().addActivity(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        ButterKnife.bind(this);
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
        ButterKnife.bind(this);
    }

    public void initToolBar(Toolbar toolbar, String title, int iconResId) {
        if (iconResId > 0)
            toolbar.setNavigationIcon(iconResId);
        toolbar_text.setText(title);
        setSupportActionBar(toolbar);
    }

    public void initToolBar(Toolbar toolbar, int resTitle, int iconResId) {
        initToolBar(toolbar, getString(resTitle), iconResId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActManager.getAppManager().finishAct(this);
    }
}
