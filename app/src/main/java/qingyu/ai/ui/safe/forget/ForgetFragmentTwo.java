package qingyu.ai.ui.safe.forget;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.vondear.rxtools.view.RxToast;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import qingyu.ai.GlobalApp;
import qingyu.ai.R;
import qingyu.ai.base.BaseBlockingStep;
import qingyu.ai.base.BaseEditTextListener;
import qingyu.ai.common.Constants;
import qingyu.ai.common.net.NetRequestAdapter;
import qingyu.ai.common.net.NetworkRequestCalback;
import qingyu.ai.common.net.APIFactory;
import qingyu.ai.common.net.RetrofitHttpUtil;
import qingyu.ai.common.util.CountDownButtonHolder;
import qingyu.ai.domain.PayloadBean;
import qingyu.ai.ui.safe.OnProceedListener;

/**
 * 注册页面1
 */
public class ForgetFragmentTwo extends BaseBlockingStep implements NetworkRequestCalback<PayloadBean> {
    private OnProceedListener proceedListener;

    @BindView(R.id.code_register)
    TextInputLayout code_register;//验证码
    @BindView(R.id.next_step)
    Button next_step;//下一步
    @BindView(R.id.resend)
    Button resend;//重新发送
    private CountDownButtonHolder helper;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register_fragment_two, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        Log.e("xixi", "verifyStep");
        String code = code_register.getEditText().getText().toString();
        if (TextUtils.isEmpty(code)) {
            return new VerificationError("验证码不能为空");
        }
        return null;
    }


    @Override
    public void onError(@NonNull VerificationError error) {
        code_register.setError("验证码不能为空");
    }

    @Override
    public void onSelected() {
        helper = new CountDownButtonHolder(resend, "重新发送", 60, 1);
        helper.startCD();
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        GlobalApp.code = code_register.getEditText().getText().toString();
        callback.goToNextStep();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnProceedListener)
            this.proceedListener = (OnProceedListener) context;
        else
            throw new IllegalStateException("Activity must implement OnProceedListener");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helper.startCD();
                HashMap<String, String> params = new HashMap<>();
                params.put("account", GlobalApp.account);
                APIFactory responseHandle = new APIFactory(getContext(),RetrofitHttpUtil.BASE_URL);
                responseHandle.getForGetPassCode(params, new NetRequestAdapter<PayloadBean>(ForgetFragmentTwo.this));
            }
        });
        next_step.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proceedListener.onProceed();
            }
        });
        code_register.getEditText().addTextChangedListener(new BaseEditTextListener() {
            @Override
            public void afterTextChanged(Editable s) {
                code_register.setError(null);
            }
        });
    }

    @Override
    public void onSuccess(PayloadBean data) {
        RxToast.success("请求验证码成功，请注意查收！");
    }

    @Override
    public void onError(int errorCode, String errorMsg) {
        if (errorCode == Constants.net.VERIFICATION_CODE_TOO_MORE) {
            RxToast.error(errorMsg);
        } else {
            RxToast.error("验证码获取失败，请重新获取验证码！");
        }
    }

    @Override
    public void onNetStart() {

    }

    @Override
    public void onNetFinish() {

    }
}
