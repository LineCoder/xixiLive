package qingyu.ai.ui.safe.password;

import android.content.Intent;
import android.os.Bundle;

import com.stepstone.stepper.StepperLayout;

import qingyu.ai.R;
import qingyu.ai.adapter.ChangePassStepperAdapter;
import qingyu.ai.base.BaseActivity2;
import qingyu.ai.ui.personal.PersonalActivity;
import qingyu.ai.ui.safe.OnProceedListener;

public class ChangePasswordActivity extends BaseActivity2 implements OnProceedListener {
    private StepperLayout stepperLayout;
    private ChangePassStepperAdapter stepperAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initToolBar(getString(R.string.change_pass_title), R.mipmap.navbar_icon_back);
        stepperLayout = findViewById(R.id.stepperLayout);
        stepperAdapter = new ChangePassStepperAdapter(getSupportFragmentManager(), this);
        stepperLayout.setAdapter(stepperAdapter);
    }

    @Override
    public void onProceed() {
        stepperLayout.proceed();
    }

    @Override
    public void back() {
        startActivity(new Intent(this, PersonalActivity.class));
    }

    @Override
    public void onBackPressed() {
        back();
    }
}
