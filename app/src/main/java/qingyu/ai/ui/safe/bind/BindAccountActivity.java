package qingyu.ai.ui.safe.bind;

import android.os.Bundle;

import com.stepstone.stepper.StepperLayout;

import qingyu.ai.R;
import qingyu.ai.adapter.BindAccountStepperAdapter;
import qingyu.ai.adapter.RegisterStepperAdapter;
import qingyu.ai.base.BaseActivity2;
import qingyu.ai.ui.safe.OnProceedListener;

public class BindAccountActivity extends BaseActivity2 implements OnProceedListener {
    private StepperLayout stepperLayout;
    private BindAccountStepperAdapter stepperAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initToolBar(getString(R.string.bind_title), R.mipmap.navbar_icon_back);
        stepperLayout = findViewById(R.id.stepperLayout);
        stepperAdapter = new BindAccountStepperAdapter(getSupportFragmentManager(), this);
        stepperLayout.setAdapter(stepperAdapter);
    }

    @Override
    public void onProceed() {
        stepperLayout.proceed();
    }
}