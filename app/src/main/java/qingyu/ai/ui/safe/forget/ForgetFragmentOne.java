package qingyu.ai.ui.safe.forget;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.vondear.rxtools.view.RxToast;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import qingyu.ai.GlobalApp;
import qingyu.ai.R;
import qingyu.ai.base.BaseBlockingStep;
import qingyu.ai.base.BaseEditTextListener;
import qingyu.ai.common.net.APIFactory;
import qingyu.ai.common.net.NetRequestAdapter;
import qingyu.ai.common.net.NetworkRequestCalback;
import qingyu.ai.common.net.RetrofitHttpUtil;
import qingyu.ai.common.util.RegexUtils;
import qingyu.ai.domain.PayloadBean;
import qingyu.ai.ui.safe.OnProceedListener;

/**
 * 注册页面1
 */
public class ForgetFragmentOne extends BaseBlockingStep implements NetworkRequestCalback<PayloadBean> {

    private OnProceedListener proceedListener;
    private StepperLayout.OnNextClickedCallback callback;
    private boolean requesting = false;//正在请求网络

    @BindView(R.id.next_step)
    Button next_step;
    @BindView(R.id.mobile_register)
    TextInputLayout mobile_register;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register_fragment_one, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    /**
     * 如果验证错误，会回调onError，否则回调onNextClicked
     *
     * @return
     */
    @Nullable
    @Override
    public VerificationError verifyStep() {
        String account = mobile_register.getEditText().getText().toString().trim();
        if (!RegexUtils.isMobileExact(account) && !RegexUtils.isEmail(account)) {
            return new VerificationError("请输入正确的手机号或邮箱");
        }
        return null;
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Log.e("xixi", "onError");
        mobile_register.setError("请输入正确的手机号或邮箱");
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        Log.e("xixi", "onNextClicked");
        if (!requesting) {
            this.requesting = true;
            this.callback = callback;
            HashMap<String, String> params = new HashMap<>();
            params.put("account", mobile_register.getEditText().getText().toString());
            APIFactory responseHandle = new APIFactory(getActivity(), RetrofitHttpUtil.BASE_URL);
            responseHandle.getForGetPassCode(params, new NetRequestAdapter<PayloadBean>(this));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnProceedListener)
            this.proceedListener = (OnProceedListener) context;
        else
            throw new IllegalStateException("Activity must implement OnProceedListener");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        next_step.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proceedListener.onProceed();
            }
        });
        mobile_register.getEditText().addTextChangedListener(new BaseEditTextListener() {
            @Override
            public void afterTextChanged(Editable s) {
                mobile_register.setError(null);
            }
        });
    }

    /**
     * 请求验证码成功
     *
     * @param data
     */
    @Override
    public void onSuccess(PayloadBean data) {
        this.callback.goToNextStep();//请求成功跳转下一步
        //携带手机或邮箱号，进入第二部
        GlobalApp.account = mobile_register.getEditText().getText().toString();
        this.requesting = false;
        RxToast.success("请求验证码成功，请注意查收！");
    }

    @Override
    public void onError(int errorCode, String errorMsg) {
        RxToast.error(errorMsg);
        this.requesting = false;
    }

    @Override
    public void onNetStart() {

    }

    @Override
    public void onNetFinish() {

    }
}
