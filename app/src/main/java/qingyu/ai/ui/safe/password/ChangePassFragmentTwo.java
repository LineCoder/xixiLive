package qingyu.ai.ui.safe.password;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.vondear.rxtools.view.RxToast;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import qingyu.ai.GlobalApp;
import qingyu.ai.R;
import qingyu.ai.base.BaseBlockingStep;
import qingyu.ai.base.BaseEditTextListener;
import qingyu.ai.common.Constants;
import qingyu.ai.common.net.APIFactory;
import qingyu.ai.common.net.NetRequestAdapter;
import qingyu.ai.common.net.NetworkRequestCalback;
import qingyu.ai.common.net.RetrofitHttpUtil;
import qingyu.ai.common.util.EncryptUtil;
import qingyu.ai.common.util.SPUtil;
import qingyu.ai.domain.UserInfoData;
import qingyu.ai.ui.personal.PersonalActivity;
import qingyu.ai.ui.safe.login.LoginActivity;
import qingyu.ai.ui.safe.OnProceedListener;

/**
 * 注册页面1
 */
public class ChangePassFragmentTwo extends BaseBlockingStep implements NetworkRequestCalback<UserInfoData> {
    @BindView(R.id.pass_1)
    TextInputLayout pass1;
    @BindView(R.id.pass_2)
    TextInputLayout pass2;
    @BindView(R.id.btn_done)
    Button btnDone;
    private OnProceedListener proceedListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.change_pass_fragment_two, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        String pass1_content = pass1.getEditText().getText().toString();
        String pass2_content = pass2.getEditText().getText().toString();
        if (pass1_content.length() < 9) {
            pass1.setError("密码为大于8位的数字和字母");
            return new VerificationError("密码为大于8位的数字和字母");
        }
        if (pass2_content.length() < 9) {
            pass2.setError("密码为大于8位的数字和字母");
            return new VerificationError("密码为大于8位的数字和字母");
        }

        if (!pass1_content.equals(pass2_content)) {
            RxToast.error("两次密码不一致");
            return new VerificationError("两次密码不一致");
        }
        return null;
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
        Log.e("xixi", GlobalApp.oldpass);
        String passText = pass2.getEditText().getText().toString();
        String passwordEncrypt = EncryptUtil.encryptMD5ToString(passText).toLowerCase();
        String oldPasswordEncrypt = EncryptUtil.encryptMD5ToString(GlobalApp.oldpass).toLowerCase();
        Map<String, String> map = new HashMap<>();
        map.put("token", SPUtil.getString(Constants.userinfo.access_token, ""));
        map.put("old_password", oldPasswordEncrypt);
        map.put("password", passwordEncrypt);
        new APIFactory(getActivity(), RetrofitHttpUtil.BASE_URL)
                .updatePassword(map, new NetRequestAdapter<UserInfoData>(this));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnProceedListener)
            this.proceedListener = (OnProceedListener) context;
        else
            throw new IllegalStateException("Activity must implement OnProceedListener");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //刷新Stepper的回调对应方法
                proceedListener.onProceed();
            }
        });


        pass1.getEditText().addTextChangedListener(new BaseEditTextListener() {
            @Override
            public void afterTextChanged(Editable s) {
                pass1.setError(null);
            }
        });

        pass2.getEditText().addTextChangedListener(new BaseEditTextListener() {
            @Override
            public void afterTextChanged(Editable s) {
                pass2.setError(null);
            }
        });
    }

    @Override
    public void onSuccess(UserInfoData data) {
        RxToast.success("密码修改成功,请重新登陆");
        startActivity(new Intent(getActivity(), LoginActivity.class));
        getActivity().finish();
    }

    @Override
    public void onError(int errorCode, String errorMsg) {
        startActivity(new Intent(getActivity(), PersonalActivity.class));
        getActivity().finish();
        RxToast.error(errorMsg);
        btnDone.setEnabled(true);
    }

    @Override
    public void onNetStart() {
        btnDone.setEnabled(false);
    }

    @Override
    public void onNetFinish() {
        btnDone.setEnabled(true);
    }
}
