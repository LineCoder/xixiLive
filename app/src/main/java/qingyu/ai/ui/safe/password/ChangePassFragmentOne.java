package qingyu.ai.ui.safe.password;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import butterknife.BindView;
import butterknife.ButterKnife;
import qingyu.ai.GlobalApp;
import qingyu.ai.R;
import qingyu.ai.base.BaseBlockingStep;
import qingyu.ai.base.BaseEditTextListener;
import qingyu.ai.common.Constants;
import qingyu.ai.common.util.SPUtil;
import qingyu.ai.ui.safe.OnProceedListener;

/**
 * 注册页面1
 */
public class ChangePassFragmentOne extends BaseBlockingStep {
    private OnProceedListener proceedListener;


    @BindView(R.id.old_account)
    TextInputLayout oldAccount;
    @BindView(R.id.old_pass)
    TextInputLayout oldPass;
    @BindView(R.id.next_step)
    Button nextStep;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.change_pass_fragment_one, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    /**
     * 如果验证错误，会回调onError，否则回调onNextClicked
     *
     * @return
     */
    @Nullable
    @Override
    public VerificationError verifyStep() {
        if (oldPass.getEditText().getText().length() < 9) {
            return new VerificationError("密码为大于8位的数字和字母");
        }
        return null;
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        oldPass.setError("密码为大于8位的数字和字母");
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        Log.e("xixi", "onNextClicked");
        GlobalApp.oldpass = oldPass.getEditText().getText().toString();
        callback.goToNextStep();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnProceedListener)
            this.proceedListener = (OnProceedListener) context;
        else
            throw new IllegalStateException("Activity must implement OnProceedListener");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String account = SPUtil.getString(Constants.userinfo.account, "");
        oldAccount.getEditText().setText(account);
        oldAccount.setEnabled(false);

        nextStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proceedListener.onProceed();
            }
        });

        oldPass.getEditText().addTextChangedListener(new BaseEditTextListener() {
            @Override
            public void afterTextChanged(Editable s) {
                oldPass.setError(null);
            }
        });
    }
}
