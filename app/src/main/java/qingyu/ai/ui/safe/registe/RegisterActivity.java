package qingyu.ai.ui.safe.registe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.stepstone.stepper.StepperLayout;

import butterknife.BindView;
import qingyu.ai.R;
import qingyu.ai.adapter.RegisterStepperAdapter;
import qingyu.ai.base.BaseActivity2;
import qingyu.ai.ui.safe.OnProceedListener;

public class RegisterActivity extends BaseActivity2 implements OnProceedListener {
    private StepperLayout stepperLayout;
    private RegisterStepperAdapter stepperAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initToolBar(getString(R.string.register_title), R.mipmap.navbar_icon_back);
        stepperLayout = findViewById(R.id.stepperLayout);
        stepperAdapter = new RegisterStepperAdapter(getSupportFragmentManager(), this);
        stepperLayout.setAdapter(stepperAdapter);
    }

    @Override
    public void onProceed() {
        stepperLayout.proceed();
    }
}
