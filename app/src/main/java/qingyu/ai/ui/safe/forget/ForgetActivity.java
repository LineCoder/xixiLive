package qingyu.ai.ui.safe.forget;

import android.os.Bundle;

import com.stepstone.stepper.StepperLayout;

import qingyu.ai.R;
import qingyu.ai.adapter.ForgetStepperAdapter;
import qingyu.ai.adapter.RegisterStepperAdapter;
import qingyu.ai.base.BaseActivity2;
import qingyu.ai.ui.safe.OnProceedListener;

public class ForgetActivity extends BaseActivity2 implements OnProceedListener {
    private StepperLayout stepperLayout;
    private ForgetStepperAdapter stepperAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initToolBar(getString(R.string.forget_title), R.mipmap.navbar_icon_back);
        stepperLayout = findViewById(R.id.stepperLayout);
        stepperAdapter = new ForgetStepperAdapter(getSupportFragmentManager(), this);
        stepperLayout.setAdapter(stepperAdapter);
    }

    @Override
    public void onProceed() {
        stepperLayout.proceed();
    }
}
