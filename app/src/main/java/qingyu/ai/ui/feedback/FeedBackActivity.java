package qingyu.ai.ui.feedback;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jph.takephoto.app.TakePhoto;
import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.compress.CompressConfig;
import com.jph.takephoto.model.TImage;
import com.jph.takephoto.model.TResult;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import qingyu.ai.adapter.FeeadBackAdapter;
import qingyu.ai.R;

import static android.widget.GridLayout.VERTICAL;

public class FeedBackActivity extends TakePhotoActivity implements View.OnClickListener {
    private int position = -1;
    @BindView(R.id.toolbar_text)
    TextView toolbar_text;
    @BindView(R.id.recycler_picture)
    RecyclerView recycler_picture;
    @BindView(R.id.navbar_icon_back)
    ImageView navbar_icon_back;
    private GridLayoutManager gridLayoutManager;
    private FeeadBackAdapter adapter;
    private ArrayList<String> imgUri = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
        initReyclerView();
    }

    private void initReyclerView() {
        recycler_picture.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(this, 5, VERTICAL, false);
        recycler_picture.setLayoutManager(gridLayoutManager);
        adapter = new FeeadBackAdapter(this);
        adapter.setListener(this);
        adapter.setItemClickListener(new FeeadBackAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int p) {
                position = p;
                initTakePhoto(getTakePhoto(), 1);
            }
        });
        recycler_picture.setAdapter(adapter);
    }

    private void initViews() {
        setContentView(R.layout.activity_feed_back);
        ButterKnife.bind(this);
        navbar_icon_back.setImageResource(R.mipmap.navbar_icon_back);
        toolbar_text.setText(getString(R.string.feed_back));
    }

    @OnClick(R.id.navbar_icon_back)
    public void back() {
        finish();
    }

    @Override
    public void onClick(View v) {
        initTakePhoto(getTakePhoto(), 4 - imgUri.size());
    }

    @OnClick(R.id.feed_commit)
    public void submit() {
    }


    private void initTakePhoto(TakePhoto takePhoto, int i) {
        CompressConfig compressConfig =
                new CompressConfig.Builder()
                        .setMaxSize(50 * 1024)
                        .setMaxPixel(800)
                        .enableReserveRaw(false).create();
        takePhoto.onEnableCompress(compressConfig, false);
        takePhoto.onPickMultiple(i);
    }

    @Override
    public void takeSuccess(TResult result) {
        super.takeSuccess(result);
        ArrayList<TImage> images = result.getImages();
        if (position > 0) {
            String originalPath = images.get(0).getOriginalPath();
            imgUri.remove(position);
            imgUri.add(position, originalPath);
            Log.e("xixi", imgUri.size() + "---" + position);
            position = -1;
        } else {
            for (int i = 0; i < images.size(); i++) {
                String originalPath = images.get(i).getOriginalPath();
                imgUri.add(i, originalPath);
            }
        }
        Log.e("xixi", imgUri.size() + "---");
        adapter.setUris(imgUri);
    }


    @Override
    public void takeFail(TResult result, String msg) {
        super.takeFail(result, msg);
    }

    @Override
    public void takeCancel() {
        super.takeCancel();
    }

}
