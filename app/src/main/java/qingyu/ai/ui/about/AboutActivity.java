package qingyu.ai.ui.about;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import qingyu.ai.R;
import qingyu.ai.base.BaseActivity2;

public class AboutActivity extends BaseActivity2 {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        initToolBar(getString(R.string.about_us), R.mipmap.navbar_icon_back);
    }

}
