package qingyu.ai.ui.guide;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro2;

import qingyu.ai.ui.main.MainActivity;

public class GuideActivity extends AppIntro2 {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addSlide(new GuideTwoFragment());
        addSlide(new GuideThreeFragment());
        addSlide(new GuideFourFragment());
        addSlide(new GuideFiveFragment());

        showStatusBar(false);
        showSkipButton(false);
        setProgressButtonEnabled(true);
        setFadeAnimation();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
