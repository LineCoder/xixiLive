package qingyu.ai.ui.settings;

import android.os.Bundle;
import android.util.Log;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import butterknife.BindView;
import qingyu.ai.R;
import qingyu.ai.base.BaseActivity2;
import qingyu.ai.common.Constants;
import qingyu.ai.common.util.SPUtil;

public class PersonalVoicerActivity extends BaseActivity2 implements RadioGroup.OnCheckedChangeListener {
    @BindView(R.id.child)
    RadioButton btn_child;
    @BindView(R.id.women)
    RadioButton btn_women;
    @BindView(R.id.man)
    RadioButton btn_man;
    @BindView(R.id.radio)
    RadioGroup radio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_voicer);
        initToolBar(getString(R.string.voice_type), R.mipmap.navbar_icon_back);
        radio.setOnCheckedChangeListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String voicer = SPUtil.getString(Constants.settings.VOICER, "vinn");
        RadioButton rb = "vinn".equals(voicer) ? btn_child : ("xiaoqi".equals(voicer) ? btn_women : btn_man);
        rb.setChecked(true);
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        String voicer = "";
        switch (checkedId) {
            case R.id.women:
                voicer = btn_women.getTag().toString();
                break;
            case R.id.man:
                voicer = btn_man.getTag().toString();
                break;
            case R.id.child:
                voicer = btn_child.getTag().toString();
                break;
        }
        SPUtil.putString(Constants.settings.VOICER, voicer);
        group.check(checkedId);
        Log.e("xixi", "" + checkedId);
    }
}
