package qingyu.ai.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Switch;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import qingyu.ai.R;
import qingyu.ai.base.BaseActivity2;
import qingyu.ai.common.Constants;
import qingyu.ai.common.speech.synthesizer.SpeechSynthesizerWrapper;
import qingyu.ai.common.util.SPUtil;

public class SettingsActivity extends BaseActivity2 {

    @BindView(R.id.wake_up_word)
    TextView wake_up_word;
    @BindView(R.id.speech_switch)
    Switch speech_switch;
    @BindView(R.id.speech_voice_name)
    TextView speech_voice_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initToolBar(getString(R.string.setting), R.mipmap.navbar_icon_back);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        //唤醒
        boolean wakeSwitch = SPUtil.getBoolean(Constants.settings.WAKE_SWITCH, false);
        StringBuffer sb = new StringBuffer("");
        if (wakeSwitch) {
            String word1 = SPUtil.getString(Constants.settings.CHECKED_WAKEWORD1, "");
            String word2 = SPUtil.getString(Constants.settings.CHECKED_WAKEWORD2, "");

            if (!TextUtils.isEmpty(word1)) {
                sb.append(word1);
            }
            if (!TextUtils.isEmpty(word1) && !TextUtils.isEmpty(word2)) {
                sb.append(",");
            }
            if (!TextUtils.isEmpty(word2)) {
                sb.append(word2);
            }
        }
        wake_up_word.setText(sb.toString());

        //语音是否播报
        speech_switch.setChecked(SPUtil.getBoolean(Constants.settings.IS_SYNTHES_PLAY, true));
        String voicer = SPUtil.getString(Constants.settings.VOICER, "vinn");
        String voicer_text = "vinn".equals(voicer) ? "童声" : ("xiaoqi".equals(voicer) ? "女声" : "男声");
        speech_voice_name.setText(voicer_text);
    }

    @OnClick(R.id.settings_wake_up)
    public void settingItemWakeUp() {
        startActivity(new Intent(this, SettingWakeUpActivity.class));
    }

    @OnCheckedChanged(R.id.speech_switch)
    public void speechSwitch(boolean isChecked) {
        if (!isChecked)
            SpeechSynthesizerWrapper.createSynthesizerCommond(this).getSynthesizer().pauseSpeaking();
        else
            SpeechSynthesizerWrapper.createSynthesizerCommond(this).getSynthesizer().resumeSpeaking();

        SPUtil.putBoolean(Constants.settings.IS_SYNTHES_PLAY, isChecked);
        speech_switch.setChecked(isChecked);
    }

    @OnClick(R.id.speech_voice)
    public void speechVoicer() {
        startActivity(new Intent(this, PersonalVoicerActivity.class));
    }

    @OnClick(R.id.clear_cache)
    public void clearCache() {
        Log.e("xixi", "clear_cache");
    }

    @OnClick(R.id.clear_history)
    public void clearHistory() {
        Log.e("xixi", "clear_history");
    }
}
