package qingyu.ai.ui.settings;

import android.support.constraint.Group;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Switch;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import qingyu.ai.R;
import qingyu.ai.base.BaseActivity2;
import qingyu.ai.common.Constants;
import qingyu.ai.common.util.SPUtil;

public class SettingWakeUpActivity extends BaseActivity2 {

    @BindView(R.id.group)
    Group group;
    @BindView(R.id.wake_up_switch)
    Switch wake_up_switch;
    @BindView(R.id.wake_word_index_one)
    CheckBox wake_word_index_one;
    @BindView(R.id.wake_word_index_two)
    CheckBox wake_word_index_two;
    @BindView(R.id.wake_word_index2)
    TextView wake_word_index2;
    @BindView(R.id.wake_word_index)
    TextView wake_word_index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_wake_up);
        initToolBar(getString(R.string.wake_up), R.mipmap.navbar_icon_back);
        setWakeUpSwitch();
    }

    @OnCheckedChanged(R.id.wake_up_switch)
    public void wakeUpSwitch(boolean isChecked) {
        SPUtil.putBoolean(Constants.settings.WAKE_SWITCH, isChecked);
        wake_up_switch.setChecked(isChecked);
        group.setVisibility(!isChecked ? View.GONE : View.VISIBLE);
    }


    private void setWakeUpSwitch() {
        boolean wakeSwitch = SPUtil.getBoolean(Constants.settings.WAKE_SWITCH, false);
        group.setVisibility(!wakeSwitch ? View.GONE : View.VISIBLE);
        wake_up_switch.setChecked(wakeSwitch);
        wake_word_index_one.setChecked(!TextUtils.isEmpty(SPUtil.getString(Constants.settings.CHECKED_WAKEWORD1, "")));
        wake_word_index_two.setChecked(!TextUtils.isEmpty(SPUtil.getString(Constants.settings.CHECKED_WAKEWORD2, "")));

    }

    @OnCheckedChanged(R.id.wake_word_index_two)
    public void wakeupWordTwo(boolean isChecked) {
        if (!isChecked) {
            SPUtil.remove(Constants.settings.CHECKED_WAKEWORD2);
        } else {
            SPUtil.putString(Constants.settings.CHECKED_WAKEWORD2, wake_word_index2.getText().toString());
        }
        wake_word_index_two.setChecked(isChecked);
    }

    @OnCheckedChanged(R.id.wake_word_index_one)
    public void wakeupWordOne(boolean isChecked) {
        if (!isChecked) {
            SPUtil.remove(Constants.settings.CHECKED_WAKEWORD1);
        } else {
            SPUtil.putString(Constants.settings.CHECKED_WAKEWORD1, wake_word_index.getText().toString());
        }
        wake_word_index_one.setChecked(isChecked);
    }
}
