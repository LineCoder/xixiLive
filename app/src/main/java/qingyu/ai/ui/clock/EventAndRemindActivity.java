package qingyu.ai.ui.clock;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import qingyu.ai.R;
import qingyu.ai.base.BaseActivity2;

public class EventAndRemindActivity extends BaseActivity2 {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_and_remind);
        initToolBar(getString(R.string.clock_title), R.mipmap.navbar_icon_back);
    }

}
